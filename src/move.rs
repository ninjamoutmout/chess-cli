use std::io;
use crate::Chessboard;
use crate::Color;
use crate::l_move_transcripter;
use crate::a_move_transcripter;


pub fn p_move(playing_color: Color, current_board: Chessboard) -> Vec<usize>{

    if playing_color == Color::White {
        println!("White to play");
    }
    else {
        println!("Black to play");
    }
    
    let o: char = 'S'; 


    let mut m_attempt = String::new();
    io::stdin().read_line(&mut m_attempt).expect("failed to read line");

    let mut coord_buffer: Vec<char> = m_attempt.chars().collect();
    coord_buffer.pop();

    match &coord_buffer[..] {
        //literal moves
        [xf@'a'..='h', yf@'1'..='8', xt@'a'..='h', yt@'1'..='8'] => l_move_transcripter::litteral_to_computer(*xf, *yf, *xt, *yt, o),
        // [yf@'a'..='h', xf@'1'..='8', yt@'a'..='h', xt@'1'..='8', '+'] => l_move_transcripter::litteral_to_computer(*yf, *xf, *yt, *xt, o),
        // [yf@'a'..='h', xf@'1'..='8', yt@'a'..='h', xt@'1'..='8', '+', '+'] => l_move_transcripter::litteral_to_computer(*yf, *xf, *yt, *xt, o),
        // [yf@'a'..='h', xf@'1'..='8', yt@'a'..='h', xt@'1'..='8', '#'] => l_move_transcripter::litteral_to_computer(*yf, *xf, *yt, *xt, o),
        //With promotions
        // [yf@'a'..='h', xf@'1'..='8', yt@'a'..='h', xt@'1'..='8', '=', promoted_to@('Q'|'R'|'B'|'N')] => l_move_transcripter::litteral_to_computer(*yf, *xf, *yt, *xt, *promoted_to),
        // [yf@'a'..='h', xf@'1'..='8', yt@'a'..='h', xt@'1'..='8', '=', promoted_to@('Q'|'R'|'B'|'N'), '+'] => l_move_transcripter::litteral_to_computer(*yf, *xf, *yt, *xt, *promoted_to),
        // [yf@'a'..='h', xf@'1'..='8', yt@'a'..='h', xt@'1'..='8', '=', promoted_to@('Q'|'R'|'B'|'N'), '+', '+'] => l_move_transcripter::litteral_to_computer(*yf, *xf, *yt, *xt, *promoted_to),
        // [yf@'a'..='h', xf@'1'..='8', yt@'a'..='h', xt@'1'..='8', '=', promoted_to@('Q'|'R'|'B'|'N'), '#'] => l_move_transcripter::litteral_to_computer(*yf, *xf, *yt, *xt, *promoted_to),



        //algebraic anything else moves
        [moved_piece@('Q'|'R'|'B'|'N'|'K'), xt@'a'..='h', yt@'1'..='8'] => a_move_transcripter::general_algebraic_to_computer(*moved_piece, *xt, *yt, playing_color, current_board), 
        [moved_piece@('Q'|'R'|'B'|'N'|'K'), xt@'a'..='h', yt@'1'..='8', '+'] =>a_move_transcripter::general_algebraic_to_computer(*moved_piece, *xt, *yt, playing_color, current_board),  
        [moved_piece@('Q'|'R'|'B'|'N'|'K'), xt@'a'..='h', yt@'1'..='8', '+', '+'] => a_move_transcripter::general_algebraic_to_computer(*moved_piece, *xt, *yt, playing_color, current_board), 
        [moved_piece@('Q'|'R'|'B'|'N'|'K'), xt@'a'..='h', yt@'1'..='8', '#'] => a_move_transcripter::general_algebraic_to_computer(*moved_piece, *xt, *yt, playing_color, current_board), 

        //specifical algebraic moves (rank)
        [moved_piece@('Q'|'R'|'B'|'N'|'K'), yf@('1'..='8'), xt@'a'..='h', yt@'1'..='8'] => a_move_transcripter::rank_algebraic_to_computer(*moved_piece, *xt, *yt, playing_color, current_board, *yf),
        // [moved_piece@('Q'|'R'|'B'|'N'), rank@('1'..='8'), 'a'..='h', '1'..='8', '+'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {rank} rd rank, with a check"),
        // [moved_piece@('Q'|'R'|'B'|'N'), rank@('1'..='8'), 'a'..='h', '1'..='8', '+', '+'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {rank} rd rank, with a double check"),
        // [moved_piece@('Q'|'R'|'B'|'N'), rank@('1'..='8'), 'a'..='h', '1'..='8', '#'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {rank} rd rank, with a checkmate ?"),

        //specifical algebraic moves (file)
        [moved_piece@('Q'|'R'|'B'|'N'|'K'), xf@('a'..='h'), xt@'a'..='h', yt@'1'..='8'] => a_move_transcripter::file_algebraic_to_computer(*moved_piece, *xt, *yt, playing_color, current_board, *xf),
        // [moved_piece@('Q'|'R'|'B'|'N'), file@('a'..='h'), 'a'..='h', '1'..='8', '+'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {file} file, with a check"),
        // [moved_piece@('Q'|'R'|'B'|'N'), file@('a'..='h'), 'a'..='h', '1'..='8', '+', '+'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {file} file, with a double check"),
        // [moved_piece@('Q'|'R'|'B'|'N'), file@('a'..='h'), 'a'..='h', '1'..='8', '#'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {file} file, with a checkmate ?"),


        //algebraic moves with capture
        //algebraic anything else moves
        // [moved_piece@('Q'|'R'|'B'|'N'|'K'), 'x', yt@'a'..='h', xt@'1'..='8'] => a_move_transcripter::general_algebraic_to_computer(*moved_piece, *xt, *yt, is_white_to_play, current_board), 
        // [moved_piece@('Q'|'R'|'B'|'N'|'K'), 'x', yt@'a'..='h', xt@'1'..='8', '+'] => a_move_transcripter::general_algebraic_to_computer(*moved_piece, *xt, *yt, is_white_to_play, current_board), 
        // [moved_piece@('Q'|'R'|'B'|'N'|'K'), 'x', yt@'a'..='h', xt@'1'..='8', '+', '+'] =>a_move_transcripter::general_algebraic_to_computer(*moved_piece, *xt, *yt, is_white_to_play, current_board), 
        // [moved_piece@('Q'|'R'|'B'|'N'|'K'), 'x', yt@'a'..='h', xt@'1'..='8', '#'] => a_move_transcripter::general_algebraic_to_computer(*moved_piece, *xt, *yt, is_white_to_play, current_board), 

        /*
        //specifical algebraic moves (rank)
        [moved_piece@('Q'|'R'|'B'|'N'), rank@('1'..='8'), 'x', 'a'..='h', '1'..='8'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {rank} rd rank, with a capture"),
        [moved_piece@('Q'|'R'|'B'|'N'), rank@('1'..='8'), 'x', 'a'..='h', '1'..='8', '+'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {rank} rd rank, with a capture, with a check"),
        [moved_piece@('Q'|'R'|'B'|'N'), rank@('1'..='8'), 'x', 'a'..='h', '1'..='8', '+', '+'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {rank} rd rank, with a capture, with a double check"),
        [moved_piece@('Q'|'R'|'B'|'N'), rank@('1'..='8'), 'x', 'a'..='h', '1'..='8', '#'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {rank} rd rank, with a capture, with a checkmate ?"),

        //specifical algebraic moves (file)
        [moved_piece@('Q'|'R'|'B'|'N'), file@('a'..='h'), 'x', 'a'..='h', '1'..='8'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {file} file, with a capture"),
        [moved_piece@('Q'|'R'|'B'|'N'), file@('a'..='h'), 'x', 'a'..='h', '1'..='8', '+'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {file} file, with a capture, with a check"),
        [moved_piece@('Q'|'R'|'B'|'N'), file@('a'..='h'), 'x', 'a'..='h', '1'..='8', '+', '+'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {file} file, with a capture, with a double check"),
        [moved_piece@('Q'|'R'|'B'|'N'), file@('a'..='h'), 'x', 'a'..='h', '1'..='8', '#'] => println!("f***ing algebraic user, moved a {moved_piece}, from the {file} file, with a capture, with a checkmate ?"),
        */


        _ => p_move(playing_color, current_board), 

    }
}


