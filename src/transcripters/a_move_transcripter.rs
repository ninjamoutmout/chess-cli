use crate::{Color, Piece, PieceKind, Chessboard, r#move};

pub fn general_algebraic_to_computer(moved_p_char: char, xt_char: char, yt_char: char, playing_color: Color, current_board: Chessboard) -> Vec<usize>{

    let mut xt: i64 = 0;
    let mut yt: i64 = 0;

    match xt_char {
        'a' => xt = 0,
        'b' => xt = 1,
        'c' => xt = 2,
        'd' => xt = 3,
        'e' => xt = 4,
        'f' => xt = 5,
        'g' => xt = 6,
        'h' => xt = 7,
        _ => println!("9 ?"),
        
    }

    match yt_char {
        '1' => yt = 0,
        '2' => yt = 1,
        '3' => yt = 2,
        '4' => yt = 3,
        '5' => yt = 4,
        '6' => yt = 5,
        '7' => yt = 6,
        '8' => yt = 7,
        _ => println!("i ?"),
        
    }

    let mut piece_to_move: Piece = Piece(Color::White, PieceKind::Empty);

    piece_to_move.0 = playing_color;

    match moved_p_char {
        'Q' => {
            piece_to_move.1 = PieceKind::Queen;
            queen_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color)
        },
        'K' => {
            piece_to_move.1 = PieceKind::King;
            king_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color)
        },
        'N' => {
            piece_to_move.1 = PieceKind::Knight;
            knight_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color)
        },
        'B' => {
            piece_to_move.1 = PieceKind::Bishop;
            bishop_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color)
        },
        'R' => {
            piece_to_move.1 = PieceKind::Rook;
            rook_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color)
        },
        _ => {
            unreachable!()
        },
    }

}

fn knight_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color) -> Vec<usize>{
    let mut move_info: Vec<usize> = Vec::new();

    const POSSIBLE_MOVES: [(i64, i64); 8] = [
        (2, 1),
        (2, -1),
        (-2, 1),
        (-2, -1),
        (1, 2),
        (1, -2),
        (-1, 2),
        (-1, -2),
    ];

    for (dx, dy) in POSSIBLE_MOVES{
        let pxt = (xt as i64) + dx;
        let pyt = (yt as i64) + dy;
        if pxt < 0 || pyt < 0 || pxt >= 8 || pyt >= 8{
        }
        else if cb.get_cell_info((pxt).try_into().unwrap(), (pyt).try_into().unwrap()) == &piece_to_move {
            move_info.push((pxt).try_into().unwrap());
            move_info.push((pyt).try_into().unwrap());
        }
    }

    if move_info.len() > 3 {
        println!("please specify a rank / file");
        r#move::p_move(playing_color, cb)

    }
    else if move_info.is_empty(){
        println!("{}please type a valid move", termion::cursor::Goto(1, 18));
        r#move::p_move(playing_color, cb)
    }
    else {
        move_info.push(xt);
        move_info.push(yt);
        move_info.push(0);
        move_info
    }

}



fn bishop_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color) -> Vec<usize>{
    let mut move_info: Vec<usize> = Vec::new();

    const POSSIBLE_MOVES: [(i64, i64); 4] = [
        (-1, 1),        (1, 1),

        (-1, -1),       (1, -1), 
        
    ];

    for (dx, dy) in POSSIBLE_MOVES{
        for i in 1..=8 {
            let pxt = (xt as i64) + dx * i;
            let pyt = (yt as i64) + dy * i;
            if pxt < 0 || pyt < 0 || pxt >= 8 || pyt >= 8{
                // println!("{pxt}, {pyt} unauthorized value");
            }
            else if cb.get_cell_info((pxt).try_into().unwrap(), (pyt).try_into().unwrap()) == &piece_to_move {
                move_info.push((pxt).try_into().unwrap());
                move_info.push((pyt).try_into().unwrap());
            }
        }
    }

    if move_info.len() > 3 {
        println!("please specify a rank / file");
        r#move::p_move(playing_color, cb)

    }
    else if move_info.is_empty(){
        println!("{}please type a valid move", termion::cursor::Goto(1, 18));
        r#move::p_move(playing_color, cb)
    }
    else {
        move_info.push(xt);
        move_info.push(yt);
        move_info.push(0);
        move_info
    }

}


fn rook_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color) -> Vec<usize>{
    let mut move_info: Vec<usize> = Vec::new();

    const POSSIBLE_MOVES: [(i64, i64); 4] = [

               (1,  0),
        (0, -1),       (0, 1),
               (-1, 0),
        
    ];

    for (dx, dy) in POSSIBLE_MOVES{
            for i in 1..=8 {
            let pxt = (xt as i64) + dx * i;
            let pyt = (yt as i64) + dy * i;
            if pxt < 0 || pyt < 0 || pxt >= 8 || pyt >= 8{
                // println!("{pxt}, {pyt} unauthorized value");
            }
            else if cb.get_cell_info((pxt).try_into().unwrap(), (pyt).try_into().unwrap()) == &piece_to_move {
                move_info.push((pxt).try_into().unwrap());
                move_info.push((pyt).try_into().unwrap());
            }
        }
    }

    if move_info.len() > 3 {
        println!("please specify a rank / file");
        r#move::p_move(playing_color, cb)

    }
    else if move_info.is_empty(){
        println!("{}please type a valid move", termion::cursor::Goto(1, 18));
        r#move::p_move(playing_color, cb)
    }
    else {
        move_info.push(xt);
        move_info.push(yt);
        move_info.push(0);
        move_info
    }

}

fn queen_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color) -> Vec<usize>{
    let mut move_info: Vec<usize> = Vec::new();

    const POSSIBLE_MOVES: [(i64, i64); 8] = [
        (-1,  1), ( 0,  1), ( 1,  1),
        (-1,  0),           ( 1,  0),
        (-1, -1), ( 0, -1), ( 1, -1), 
    ];

    for (dx, dy) in POSSIBLE_MOVES{
        for i in 1..=8{
            let pxt = (xt as i64) + dx * i;
            let pyt = (yt as i64) + dy * i;
            if pxt < 0 || pyt < 0 || pxt >= 8 || pyt >= 8{
                // println!("{pxt}, {pyt} unauthorized value");
            }
            else if cb.get_cell_info((pxt).try_into().unwrap(), (pyt).try_into().unwrap()) == &piece_to_move {
                move_info.push((pxt).try_into().unwrap());
                move_info.push((pyt).try_into().unwrap());
            }
        }
    }

    if move_info.len() > 3 {
        println!("please specify a rank / file");
        r#move::p_move(playing_color, cb)

    }
    else if move_info.is_empty(){
        println!("{}please type a valid move", termion::cursor::Goto(1, 18));
        r#move::p_move(playing_color, cb)
    }
    else {
        move_info.push(xt);
        move_info.push(yt);
        move_info.push(0);
        move_info
    }

}

fn king_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color) -> Vec<usize>{
    let mut move_info: Vec<usize> = Vec::new();

    const POSSIBLE_MOVES: [(i64, i64); 8] = [

        (-1,  1), ( 0,  1), ( 1,  1),
        (-1,  0),           ( 1,  0),
        (-1, -1), ( 0, -1), ( 1, -1), 
    ];

    for (dx, dy) in POSSIBLE_MOVES{
        let pxt = (xt as i64) + dx;
        let pyt = (yt as i64) + dy;
        if pxt < 0 || pyt < 0 || pxt >= 8 || pyt >= 8{
        }
        else if cb.get_cell_info((pxt).try_into().unwrap(), (pyt).try_into().unwrap()) == &piece_to_move {
            move_info.push((pxt).try_into().unwrap());
            move_info.push((pyt).try_into().unwrap());
        }
    }

    if move_info.len() > 3 {
        println!("please specify a rank / file");
        r#move::p_move(playing_color, cb)

    }
    else if move_info.is_empty(){
        println!("{}please type a valid move", termion::cursor::Goto(1, 18));
        r#move::p_move(playing_color, cb)
    }
    else {
        move_info.push(xt);
        move_info.push(yt);
        move_info.push(0);
        move_info
    }
}


pub fn rank_algebraic_to_computer(moved_p_char: char, xt_char: char, yt_char: char, playing_color: Color, current_board: Chessboard, yf_char: char) -> Vec<usize>{

    let mut xt: i64 = 0;
    let mut yt: i64 = 0;
    let mut yf: i64 = 0; 

    match xt_char {
        'a' => xt = 0,
        'b' => xt = 1,
        'c' => xt = 2,
        'd' => xt = 3,
        'e' => xt = 4,
        'f' => xt = 5,
        'g' => xt = 6,
        'h' => xt = 7,
        _ => println!("9 ?"),
        
    }
    match yt_char {
        '1' => yt = 0,
        '2' => yt = 1,
        '3' => yt = 2,
        '4' => yt = 3,
        '5' => yt = 4,
        '6' => yt = 5,
        '7' => yt = 6,
        '8' => yt = 7,
        _ => println!("i ?"),
        
    }

    match yf_char {
        '1' => yf = 0,
        '2' => yf = 1,
        '3' => yf = 2,
        '4' => yf = 3,
        '5' => yf = 4,
        '6' => yf = 5,
        '7' => yf = 6,
        '8' => yf = 7,
        _ => println!("9 ?"),
        
    }

    let mut piece_to_move: Piece = Piece(Color::White, PieceKind::Empty);

    piece_to_move.0 = playing_color;

    match moved_p_char {
        'Q' => {
            piece_to_move.1 = PieceKind::Queen;
            return long_range_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color, (yf).try_into().unwrap());
        },
        'N' => {
            piece_to_move.1 = PieceKind::Knight;
            return knight_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color, (yf).try_into().unwrap());
        },
        'B' => {
            piece_to_move.1 = PieceKind::Bishop;
            return long_range_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color, (yf).try_into().unwrap());
        },
        'R' => {
            piece_to_move.1 = PieceKind::Rook;
            return long_range_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color, (yf).try_into().unwrap());
        }
        _ => {
            unreachable!()
        },
    }

    fn knight_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color, yf: usize) -> Vec<usize>{
        let mut move_info: Vec<usize> = Vec::new();

        const POSSIBLE_MOVES: [i64; 4] = [
            2, -2, 1, -1
        ];

        for x in POSSIBLE_MOVES{
            let pxt = (xt as i64) + x;
            if !(0..8).contains(&pxt){
            }
            else if cb.get_cell_info((pxt).try_into().unwrap(), yf) == &piece_to_move {
                move_info.push((pxt).try_into().unwrap());
                move_info.push(yf);
            }
        }

        if move_info.len() > 3 {
            println!("{}please specify a file", termion::cursor::Goto(1, 18));
            r#move::p_move(playing_color, cb)

        }
        else if move_info.is_empty(){
            println!("{}please type a valid move", termion::cursor::Goto(1, 18));
            r#move::p_move(playing_color, cb)
        }
        else {
            move_info.push(xt);
            move_info.push(yt);
            move_info.push(0);
            move_info
        }

    }


    fn long_range_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color, yf: usize) -> Vec<usize>{
        let mut move_info: Vec<usize> = Vec::new();

        const POSSIBLE_MOVES: [i64; 1] = [
            1,
        ];

        for x in POSSIBLE_MOVES{
            for i in -8..=8 {
                let pxt = (xt as i64) + x * i;
                if !(0..8).contains(&pxt){
                }
                else if cb.get_cell_info((pxt).try_into().unwrap(), yf) == &piece_to_move {
                    move_info.push((pxt).try_into().unwrap());
                    move_info.push(yf);
                }
            }
        }

            if move_info.len() > 3 {
                println!("{}please specify a file", termion::cursor::Goto(1, 18));
                return r#move::p_move(playing_color, cb);

            }
            else if move_info.is_empty(){
                println!("{}please type a valid move", termion::cursor::Goto(1, 18));
                return r#move::p_move(playing_color, cb);
            }
            else {
                move_info.push(xt);
                move_info.push(yt);
                move_info.push(0);
            }
        move_info
    }
}


pub fn file_algebraic_to_computer(moved_p_char: char, xt_char: char, yt_char: char, playing_color: Color, current_board: Chessboard, xf_char: char) -> Vec<usize>{

    let mut xt: i64 = 0;
    let mut yt: i64 = 0;
    let mut xf: i64 = 0; 

    match xt_char {
        'a' => xt = 0,
        'b' => xt = 1,
        'c' => xt = 2,
        'd' => xt = 3,
        'e' => xt = 4,
        'f' => xt = 5,
        'g' => xt = 6,
        'h' => xt = 7,
        _ => println!("9 ?"),
        
    }
    match yt_char {
        '1' => yt = 0,
        '2' => yt = 1,
        '3' => yt = 2,
        '4' => yt = 3,
        '5' => yt = 4,
        '6' => yt = 5,
        '7' => yt = 6,
        '8' => yt = 7,
        _ => println!("i ?"),
        
    }

    match xf_char {
        'a' => xf = 0,
        'b' => xf = 1,
        'c' => xf = 2,
        'd' => xf = 3,
        'e' => xf = 4,
        'f' => xf = 5,
        'g' => xf = 6,
        'h' => xf = 7,
        _ => println!("xf_char match failed"),
        
    }

    let mut piece_to_move: Piece = Piece(Color::White, PieceKind::Empty);

    piece_to_move.0 = playing_color;

    match moved_p_char {
        'Q' => {
            piece_to_move.1 = PieceKind::Queen;
            return long_range_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color, (xf).try_into().unwrap());
        },
        'N' => {
            piece_to_move.1 = PieceKind::Knight;
            return knight_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color, (xf).try_into().unwrap());
        },
        'B' => {
            piece_to_move.1 = PieceKind::Bishop;
            return long_range_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color, (xf).try_into().unwrap());
        },
        'R' => {
            piece_to_move.1 = PieceKind::Rook;
            return long_range_move(piece_to_move, current_board, (xt).try_into().unwrap(), (yt).try_into().unwrap(), playing_color, (xf).try_into().unwrap());
        }
        _ => {
            unreachable!()
        },
    }

    fn knight_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color, xf: usize) -> Vec<usize>{
        let mut move_info: Vec<usize> = Vec::new();

        const POSSIBLE_MOVES: [i64; 4] = [
            2, -2, 1, -1
        ];

        for y in POSSIBLE_MOVES{
            let pyt = (yt as i64) + y;
            if !(0..8).contains(&pyt){
            }
            else if cb.get_cell_info(xf, (pyt).try_into().unwrap()) == &piece_to_move {
                move_info.push(xf);
                move_info.push((pyt).try_into().unwrap());
            }
        }

        if move_info.len() > 3 {
            println!("please specify a rank");
            r#move::p_move(playing_color, cb)

        }
        else if move_info.is_empty(){
            println!("{}please type a valid move", termion::cursor::Goto(1, 18));
            r#move::p_move(playing_color, cb)
        }
        else {
            move_info.push(xt);
            move_info.push(yt);
            move_info.push(0);
            move_info
        }

    }


    fn long_range_move(piece_to_move: Piece, cb: Chessboard, xt: usize, yt: usize, playing_color: Color, xf: usize) -> Vec<usize>{
        let mut move_info: Vec<usize> = Vec::new();

        const POSSIBLE_MOVES: [i64; 1] = [
            1,
        ];

        for y in POSSIBLE_MOVES{
            for i in -8..=8 {
                let pyt = (yt as i64) + y * i;
                if !(0..8).contains(&pyt){
                }
                else if cb.get_cell_info(xf, (pyt).try_into().unwrap()) == &piece_to_move {
                    move_info.push(xf);
                    move_info.push((pyt).try_into().unwrap());
                }
            }
        }

            if move_info.len() > 3 {
                println!("{}please specify a file", termion::cursor::Goto(1, 18));
                return r#move::p_move(playing_color, cb);

            }
            else if move_info.is_empty(){
                println!("{}please type a valid move", termion::cursor::Goto(1, 18));
                return r#move::p_move(playing_color, cb);
            }
            else {
                move_info.push(xt);
                move_info.push(yt);
                move_info.push(0);
            }
        move_info
    }
}


