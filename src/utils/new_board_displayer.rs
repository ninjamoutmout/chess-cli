use crate::Chessboard;

pub fn display(main_board: Chessboard){
    let mut c = 0;


    print!("{}{}┌", termion::clear::All, termion::cursor::Goto(1, 1));
    // print!("┌");
    for _n in 1..=7{
        print!("─────┬")
    }
    println!("─────┐");

    for y in 0..8{
        print!("│ ");
        for x in 0..8 {
            print!("{}\x1b[97m│ ", main_board.board[x][y].render());
        }
        println!();
        c += 1;

        if c < 8 {
            print!("├─────");
            for _y in 2..=7{
                print!("\x1b[97m┼─────")
            }
            println!("┼─────┤");
        }

    }
    print!("└");
    for _n in 1..=7{
        print!("─────┴")
    }
    println!("─────┘");



}
