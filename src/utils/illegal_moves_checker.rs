use crate::{Color, Chessboard, PieceKind};
use crate::utils::king_finder;


const ORTHOGONAL: [(i64, i64); 4] = [(1, 0), (-1, 0), (0, 1), (0, -1)];
const DIAGONAL: [(i64, i64); 4] = [(1, 1), (-1, -1), (-1, 1), (1, -1)];
const KNIGHT_MOVES: [(i64, i64); 8] = [(2, 1), (2, -1), (-2, 1), (-2, -1), (1, 2), (1, -2), (-1, 2), (-1, -2)];
const OFFSET: [i64; 2] = [1 , -1];


pub fn is_move_illegal(color_to_check: Color, board_to_check: Chessboard) -> bool {

    let mut opponent_color: Color = Color::Void;

    if color_to_check == Color::Black{
        opponent_color = Color::White;
    }
    else {
        opponent_color = Color::Black;
    }
    let (x, y) = king_finder::find(board_to_check, color_to_check);

    //Queen and Rook
    for (dx, dy) in ORTHOGONAL{
        'inner : for i in 1..8{
            let xf = (x as i64) + dx * i;
            let yf = (y as i64) + dy * i;
            if (0..8).contains(&xf) && (0..8).contains(&yf) {
                let current_square = board_to_check.get_cell_info(xf as usize, yf as usize);
                if current_square.0 == opponent_color && (current_square.1 == PieceKind::Rook || current_square.1 == PieceKind::Queen){
                    return true;
                }
                else if current_square.0 == color_to_check {
                    break 'inner;
                }
            }
        }
    }
    
    //Queen and Bishop
    for (dx, dy) in DIAGONAL{
        'inner : for i in 1..8{
            let xf = (x as i64) + dx * i;
            let yf = (y as i64) + dy * i;
            if (0..8).contains(&xf) && (0..8).contains(&yf) {
                let current_square = board_to_check.get_cell_info(xf as usize, yf as usize);
                if current_square.0 == opponent_color && (current_square.1 == PieceKind::Bishop || current_square.1 == PieceKind::Queen){
                    return true;
                }
                else if current_square.0 == color_to_check {
                    break 'inner;
                }
            }
        }
    }

    //Knight
    for (dx, dy) in KNIGHT_MOVES{
        let xf = (x as i64) + dx;
        let yf = (y as i64) + dy;
        if (0..8).contains(&xf) && (0..8).contains(&yf){
            let current_square = board_to_check.get_cell_info(xf as usize, yf as usize);
            if current_square.0 == opponent_color && current_square.1 == PieceKind::Knight{
                return true;
            }
        }
    }

    //Pawn
    let mut dy: i64 = 1;
    if color_to_check == Color::Black {
        dy = -1;
    }

    for dx in OFFSET{
        let xf = (x as i64) + dx;
        let yf = (y as i64) + dy;
        if (0..8).contains(&xf) && (0..8).contains(&yf){
            let current_square = board_to_check.get_cell_info(xf as usize, yf as usize);
            if current_square.0 == opponent_color && current_square.1 == PieceKind::Pawn{
                return true;
            }
        }
    }

    false

}
