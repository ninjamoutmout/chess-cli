use crate::{Chessboard, Piece, PieceKind, Color};


pub fn find(main_board: Chessboard, king_color: Color) -> (usize, usize){
    let mut y: usize = 0;
    for (x, rank) in main_board.into_iter().enumerate() {
        for _cell in rank{
            if main_board.get_cell_info(x, y) == &Piece(king_color, PieceKind::King){
                // println!("found {:?} king at {x} {y}", king_color);
                return (x, y);
            }
            y += 1;
        }
        y = 0;
    }
    unreachable!();
}

