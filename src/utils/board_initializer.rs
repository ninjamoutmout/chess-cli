use crate::{Chessboard, PieceKind, Color, Piece};
use std::io;
use Color::Black as B;
use Color::White as W;
use Color::Void as V;

use PieceKind::Empty as E;
use PieceKind::Pawn as P;
use PieceKind::Queen as Q;
use PieceKind::King as K;
use PieceKind::Knight as N;
use PieceKind::Bishop as F;
use PieceKind::Rook as R;

pub fn init() -> Chessboard{
    let mut main_board: Chessboard = Chessboard{
    board: [
        [Piece(V, E), Piece(W, R) ,Piece(V, E), Piece(W, N), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E)],
        [Piece(V, E), Piece(V, E) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E)],
        [Piece(V, E), Piece(V, E) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E)],
        [Piece(V, E), Piece(V, E) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, K), Piece(V, E), Piece(V, E)],
        [Piece(B, K), Piece(V, E) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E)],
        [Piece(V, E), Piece(V, E) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E)],
        [Piece(V, E), Piece(V, E) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E)],
        [Piece(B, Q), Piece(V, E) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E)],
    ],
    playing_color: Color::White,
    previous_move: [0, 0, 0, 0, 0],

    };

    println!("0 for empty board");
    println!("1 for normal board");
    println!("2 for testing board");
    let mut u_input = String::new();
    io::stdin().read_line(&mut u_input).expect("Failed to read line");
    let input: u32 = u_input.trim().parse().expect("NO");

    match input{
        0 => main_board = init_empty_board(main_board),
        1 => main_board = init_king_board(main_board),
        2 => println!("ok"),
        _ => main_board = init(),

    };
    main_board


}


fn init_king_board(mut main_board: Chessboard) -> Chessboard{
    main_board.board = [
        [Piece(B, R), Piece(B, P) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, P), Piece(W, R)],
        [Piece(B, N), Piece(B, P) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, P), Piece(W, N)],
        [Piece(B, F), Piece(B, P) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, P), Piece(W, F)],
        [Piece(B, Q), Piece(B, P) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, P), Piece(W, Q)],
        [Piece(B, K), Piece(B, P) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, P), Piece(W, K)],
        [Piece(B, F), Piece(B, P) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, P), Piece(W, F)],
        [Piece(B, N), Piece(B, P) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, P), Piece(W, N)],
        [Piece(B, R), Piece(B, P) ,Piece(V, E), Piece(V, E), Piece(V, E), Piece(V, E), Piece(W, P), Piece(W, R)],
    ];
    main_board
}
fn init_empty_board(mut main_board: Chessboard) -> Chessboard{
    println!("empty board called");
    for x in 0..8{
        for y in 0..8{
            main_board.board[x][y] = Piece(W, E);
        }
    }
    main_board

}


