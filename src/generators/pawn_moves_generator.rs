use std::collections::HashSet;

use crate::{Color, Chessboard, PieceKind};


const OFFSET: [i64; 2] = [1 , -1];

pub fn generate_pawn(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{

    let mut move_list: HashSet<[usize; 5]> = HashSet::new();
    for i in normal_moves(main_board, x, y, playing_c) {
        move_list.insert(i);
    }
    for i in captures(main_board, x, y, playing_c) {
        move_list.insert(i);
    }


    move_list
}


fn normal_moves(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{
    let mut move_list: HashSet<[usize; 5]> = HashSet::new();

    let mut dy: i64 = 1;
    if playing_c == Color::Black {
        dy = -1;
    }
    let mut yt = (y as i64) + dy;
    if (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c && main_board.get_cell_info(x, yt as usize).0 == Color::Void {
            move_list.insert([x, y, x, (yt as usize), 0]); 

    }

    if playing_c == Color::White && y == 1 {
        yt = (y as i64) + 2;

        if (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c && main_board.get_cell_info(x, yt as usize).0 == Color::Void {
            move_list.insert([x, y, x, (yt as usize), 0]);
        }
    }

    if playing_c == Color::Black && y == 6 {
        yt = (y as i64) - 2;

        if (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c && main_board.get_cell_info(x, yt as usize).0 == Color::Void {
            move_list.insert([x, y, x, (yt as usize), 0]);
        }
    }

    move_list

}

fn captures(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{
    let mut move_list: HashSet<[usize; 5]> = HashSet::new();
    let mut dy: i64 = 1;
    if playing_c == Color::Black {
        dy = -1;
    }
    let yt = (y as i64) + dy;
    for dx in OFFSET {
        let xt = (x as i64) + dx;
        if (0..8).contains(&yt) && (0..8).contains(&xt) && main_board.get_cell_info(x, y).0 == playing_c && main_board.get_cell_info(xt as usize, yt as usize).0 != playing_c && main_board.get_cell_info(xt as usize, yt as usize).1 != PieceKind::Empty{
            move_list.insert([x, y, (xt as usize), (yt as usize), 0]);
        }

    }

    move_list
}

fn en_passant(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{
    todo!();
}

