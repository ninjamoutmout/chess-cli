use std::collections::HashSet;

use crate::Chessboard;
use crate::Color;
use crate::PieceKind;

use crate::pawn_moves_generator::generate_pawn;

const ORTHOGONAL: [(i64, i64); 4] = [(1, 0), (-1, 0), (0, 1), (0, -1)];
const DIAGONAL: [(i64, i64); 4] = [(1, 1), (-1, -1), (-1, 1), (1, -1)];
const KNIGHT_MOVES: [(i64, i64); 8] = [(2, 1), (2, -1), (-2, 1), (-2, -1), (1, 2), (1, -2), (-1, 2), (-1, -2)];




pub fn generate(main_board: Chessboard, playing_c: Color) -> HashSet<[usize; 5]>{
    let mut pseudo_l_move_l: HashSet<[usize; 5]> = HashSet::new();

    let mut y: usize = 0;


    for (x, rank) in main_board.board.into_iter().enumerate(){
        for _cell in rank {
            match main_board.get_cell_info(x, y).1 {
                PieceKind::Empty => {},
                PieceKind::Rook => { 
                    for i in generate_rook(main_board, x, y, playing_c){
                        pseudo_l_move_l.insert(i);
                    }
                },
                PieceKind::Bishop => {
                    for i in generate_bishop(main_board, x, y, playing_c){
                        pseudo_l_move_l.insert(i);
                    }
                },
                PieceKind::Queen => {
                    for i in generate_queen(main_board, x, y, playing_c){
                        pseudo_l_move_l.insert(i);
                    }
                },
                PieceKind::King => {
                    for i in generate_king(main_board, x, y, playing_c){
                        pseudo_l_move_l.insert(i);
                    }
                },
                PieceKind::Knight => {
                    for i in generate_knight(main_board, x, y, playing_c){
                        pseudo_l_move_l.insert(i);
                    }
                }
                PieceKind::Pawn => {
                    for i in generate_pawn(main_board, x, y, playing_c){
                        pseudo_l_move_l.insert(i);
                    }
                }
                
            }


            y += 1; 
        }
        y = 0;
    };


    pseudo_l_move_l


}

fn generate_rook(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{
    let mut move_list: HashSet<[usize; 5]> = HashSet::new();
    for (dx, dy) in ORTHOGONAL {
        'inner : for i in 1..8{
            let xt = (x as i64) + dx * i; 
            let yt = (y as i64) + dy * i; 
            if (0..8).contains(&xt) && (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c {
                if main_board.get_cell_info(xt as usize, yt as usize).0 != playing_c{
                    move_list.insert([x, y, (xt as usize), (yt as usize), 0]); 
                }
                else {
                    break 'inner;
                }
            };
        }
    }
    move_list
}

fn generate_bishop(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{
    let mut move_list: HashSet<[usize; 5]> = HashSet::new();
    for (dx, dy) in DIAGONAL {
        'inner : for i in 1..8{
            let xt = (x as i64) + dx * i; 
            let yt = (y as i64) + dy * i; 
            if (0..8).contains(&xt) && (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c{
                if main_board.get_cell_info(xt as usize, yt as usize).0 != playing_c{
                    move_list.insert([x, y, (xt as usize), (yt as usize), 0]); 
                }
                else {
                    break 'inner;
                }
            };
        }
    }
    move_list
}

fn generate_queen(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{
    let mut move_list: HashSet<[usize; 5]> = HashSet::new();
    for (dx, dy) in DIAGONAL {
        'inner : for i in 1..8{
            let xt = (x as i64) + dx * i; 
            let yt = (y as i64) + dy * i; 
            if (0..8).contains(&xt) && (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c {
                if main_board.get_cell_info(xt as usize, yt as usize).0 != playing_c{
                    move_list.insert([x, y, (xt as usize), (yt as usize), 0]); 
                }
                else {
                    break 'inner;
                }
            };
        }
    }
    for (dx, dy) in ORTHOGONAL {
        'inner : for i in 1..8{
            let xt = (x as i64) + dx * i; 
            let yt = (y as i64) + dy * i; 
            if (0..8).contains(&xt) && (0..8).contains(&yt)&& main_board.get_cell_info(x, y).0 == playing_c {
                if main_board.get_cell_info(xt as usize, yt as usize).0 != playing_c{
                    move_list.insert([x, y, (xt as usize), (yt as usize), 0]); 
                }
                else {
                    break 'inner;
                }
            };
        }
    }
    move_list
}

fn generate_king(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{
    let mut move_list: HashSet<[usize; 5]> = HashSet::new();
    for (dx, dy) in DIAGONAL {


        let xt = (x as i64) + dx;
        let yt = (y as i64) + dy;
        if (0..8).contains(&xt) && (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c && main_board.get_cell_info(xt as usize, yt as usize).0 != playing_c {
            move_list.insert([x, y, (xt as usize), (yt as usize), 0]); 
        }
    }
    for (dx, dy) in ORTHOGONAL {
        let xt = (x as i64) + dx;
        let yt = (y as i64) + dy;
        if (0..8).contains(&xt) && (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c && main_board.get_cell_info(xt as usize, yt as usize).0 != playing_c {
            move_list.insert([x, y, (xt as usize), (yt as usize), 0]); 
        }

    }
    move_list
}


fn generate_knight(main_board: Chessboard, x: usize, y: usize, playing_c: Color) -> HashSet<[usize; 5]>{
    let mut move_list: HashSet<[usize; 5]> = HashSet::new();
    for (dx, dy) in KNIGHT_MOVES {
        let xt = (x as i64) + dx;
        let yt = (y as i64) + dy;
        if (0..8).contains(&xt) && (0..8).contains(&yt) && main_board.get_cell_info(x, y).0 == playing_c && main_board.get_cell_info(xt as usize, yt as usize).0 != playing_c {
            move_list.insert([x, y, (xt as usize), (yt as usize), 0]); 
        }

    }
    move_list

}


