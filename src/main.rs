mod r#move;
mod transcripters;
mod generators;
mod utils;

use crate::utils::*;
use crate::transcripters::*;
use crate::generators::*;

// RANK IS x AND LETTERS
// FILE IS y AND NUMBERS
// EVERYTING SHOULD BE DONE BY USING x y
// EVEN NOTATION DOES

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PieceKind {
    Empty,

    Pawn,
    Queen,
    King,
    Knight,
    Bishop,
    Rook,
}

impl PieceKind {
    pub fn representation(&self) -> char {
        use PieceKind as P;
        match self {
            P::Empty => ' ',
            P::Pawn => '󰡙',
            P::Queen => '󰡚',
            P::King => '󰡗',
            P::Knight => '󰡘',
            P::Bishop => '󰡜',
            P::Rook => '󰡛',
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Color {
    White,
    Black,
    Void,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Piece(Color, PieceKind);

impl Piece {
    pub fn render(&self) -> String {
        let mut p_request = String::new();
        if self.0 == Color::Black {
            p_request.push_str("\x1b[90m");
        }
        let x = self.1.representation();
        p_request.push_str(&format!(" {x}  "));

        p_request
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Chessboard {
    board: [[Piece; 8]; 8],
    playing_color: Color,
    previous_move: [usize; 5],
    // white_k_pos: (usize, usize),
    // black_k_pos: (usize, usize),
}

impl IntoIterator for Chessboard {
    type Item = [Piece; 8];
    type IntoIter = std::array::IntoIter<[Piece; 8], 8>;

    fn into_iter(self) -> std::array::IntoIter<[Piece; 8], 8> {
        self.board.into_iter()
    }
}

impl Chessboard {
    pub fn get_cell_info(&self, x: usize, y: usize) -> &Piece {
        let selected_rank = &self.board[x]; // we do 8 - y to flip the board in white favor
        &selected_rank[7 - y] as _
    }
    

    pub fn move_maker(&mut self, move_info: Vec<usize>) -> ([[Piece; 8]; 8], [usize; 5]) {
        // new_board_displayer::display(*self);
        let mut next: Chessboard = *self;
        let xf = move_info[0];
        let yf = move_info[1];
        let xt = move_info[2];
        let yt = move_info[3];
        let piece_out: Piece;

        if self.playing_color == Color::White {
            match move_info[4] {
                0 => piece_out = *self.get_cell_info(xf, yf),
                1 => piece_out = Piece(Color::White, PieceKind::Knight),
                2 => piece_out = Piece(Color::White, PieceKind::Bishop),
                3 => piece_out = Piece(Color::White, PieceKind::Rook),
                4 => piece_out = Piece(Color::White, PieceKind::Queen),
                _ => unreachable!(),
            }
        } else {
            match move_info[4] {
                0 => piece_out = *self.get_cell_info(xf, yf),
                1 => piece_out = Piece(Color::Black, PieceKind::Knight),
                2 => piece_out = Piece(Color::Black, PieceKind::Bishop),
                3 => piece_out = Piece(Color::Black, PieceKind::Rook),
                4 => piece_out = Piece(Color::Black, PieceKind::Queen),
                _ => unreachable!(),
            }
        }
        let mut move_info_array: [usize; 5] = [0, 0, 0, 0, 0];
        move_info_array[..move_info.len()].copy_from_slice(&move_info[..]);


        if !move_list_generator::generate(*self, self.playing_color).contains(&move_info_array){
            println!("{}unallowed move", termion::cursor::Goto(1, 18));
            self.move_maker(r#move::p_move(self.playing_color, *self))
        } else {
            next.board[xt][7 - yt] = piece_out;
            next.board[xf][7 - yf] = Piece(Color::Void, PieceKind::Empty);
            match self.playing_color {
                Color::White => next.playing_color = Color::Black,
                Color::Black => next.playing_color = Color::White,
                Color::Void => unreachable!(),
                
            }
        
            if illegal_moves_checker::is_move_illegal(self.playing_color, next) {
                println!("king still in check, un allowed move");
                return self.move_maker(r#move::p_move(self.playing_color, *self));
            }
            
            self.board[xt][7 - yt] = piece_out;
            self.board[xf][7 - yf] = Piece(Color::Void, PieceKind::Empty);
            (self.board, move_info_array)
        }
    }
}

// static mut MAIN_BOARD: Chessboard;

fn main() {
    let mut main_board: Chessboard = board_initializer::init();

    new_board_displayer::display(main_board);

    loop {
        move_sequence(&mut main_board);
        if move_list_generator::generate(main_board, main_board.playing_color).is_empty(){
            println!("chekmate or no material");
        }
    }
}
pub fn move_sequence(main_board: &mut Chessboard) {

    new_board_displayer::display(*main_board);
    let x: ([[Piece; 8]; 8], [usize; 5]) = main_board.move_maker(r#move::p_move(main_board.playing_color, *main_board));
    main_board.board = x.0;
    main_board.previous_move = x.1;
    if main_board.playing_color == Color::White {
        main_board.playing_color = Color::Black
    } else {
        main_board.playing_color = Color::White
    };
}
